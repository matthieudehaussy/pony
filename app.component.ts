// import Component Decorator to be able to use it
import {Component} from '@angular/core';

//Decorate class with component Decorator, so we tell Angular that it's a component :)
@Component({
    selector: 'ponyracer-app', //identify the component in html document
    template: `
        <h1>PonyRacer</h1>
        <p [textContent]="user?.name"></p>
        <div (click)="bubblingClickRoot($event)">
            <div (click)="bubblingClick($event)">
                <ns-races #races></ns-races>
            </div>
            <div>
                <button (click)="races.start($event)">Start Races</button>
            </div>
        </div>
    `
})
export class PonyRacerComponent {
    user: any = {name: 'Crampy'};

    bubblingClick(event) {
        console.log('parent ' + event);
        event.preventDefault();
        event.stopPropagation();
    }

    bubblingClickRoot(event) {
        console.log('root ' + event);
    }
}