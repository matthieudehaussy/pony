import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {PonyRacerComponent} from './app.component';
import {RacesComponent} from './races.component';

@NgModule({
    imports: [BrowserModule], //import external module
    declarations: [PonyRacerComponent, RacesComponent], //declare our modules
    bootstrap: [PonyRacerComponent] //declare modules to instanciate at start
})
export class AppModule {

}