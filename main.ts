//app start logic
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic'; //start logic for browser side rendered app
import {AppModule} from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);