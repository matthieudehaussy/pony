import {Component} from '@angular/core';

@Component({
    selector: 'ns-races',
    template:`
        <button (click)="refreshRaces()">Refresh the races list</button>
        <div *ngIf="races.length > 0; else empty">
            <h2><p>{{races.length}} Races</p></h2>
            <ul>
                <li *ngFor="let race of races; index as i">{{i}} - {{race.name}}</li>
            </ul>
        </div>
        <ng-template #empty><p>No race</p></ng-template>
        <div [ngSwitch]="races.length">
            <p *ngSwitchCase="0">There is no race</p>
            <p *ngSwitchCase="1">There is only one race</p>
            <p *ngSwitchDefault>There are {{races.length}} races</p>
        </div>
    `
})
export class RacesComponent{
 races: any = [];
 refreshRaces(){
     this.races = [{name: 'London'}, {name: 'Lyon'}]
 }

 start(event) {
     console.log('Races start !');
     event.preventDefault();
     event.stopPropagation();
 }
}